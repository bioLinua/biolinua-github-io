document.write("<script src='/markdown/information.js'></script>");
document.write(" <script src='https://cdn.jsdelivr.net/npm/marked/marked.min.js'></script> ");
//document.write(" <script src='/node_modules/marked/marked.min.js'></script> ");

function Loading(){
    
    document.getElementById("Title").textContent = localStorage.getItem("Title");
    document.getElementById("Date").textContent = localStorage.getItem("Date");
    document.getElementById("Classify").textContent = localStorage.getItem("Classify");

    document.getElementById("Content").innerHTML = marked(load(localStorage.getItem("Read")));
}

function load(name) {
    let xhr = new XMLHttpRequest(),
        okStatus = document.location.protocol === "file:" ? 0 : 200;
    xhr.open('GET', name, false);
    xhr.overrideMimeType("text/html;charset=utf-8");//默认为utf-8
    xhr.send(null);
    return xhr.status === okStatus ? xhr.responseText : null;
}
   