document.write("<script src='/markdown/information.js'></script>");
function HomePage(){  
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/Blogs/Home.html";
}
function ClassifyPage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/Blogs/classify/index.html"
}
function AboutPage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/Blogs/about/index.html";
}
function LowHomePage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/index.html";
}
function WebAppPage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/Web_App/Home.html";
}
function PhotoPage(){
    window.location.href = 'https://github.com/bioLinua/biolinua.github.io';
}
function ReadPage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/showBlogs/showBlogs.html";
}
function WritePage(){
    localStorage.setItem("Page",document.title);
    window.location.href="/page_file/showBlogs/writeBlogs.html";
}

var open = false;
var opene = false;
var openNum = 0;

function Loading(){
    document.getElementById("announcementContent").textContent = announcementTitleContent;
}
window.onload = Loading;

function mousePhotoOn(){
    var photo = document.getElementById("photo");
    if(openNum == 0)
    {
        photo.style.animation="in_photoanimation 1s 0s";
        photo.style.animationFillMode = "forwards";
        photo.addEventListener("webkitAnimationEnd", function(){ //动画结束时事件 
            open = true
            openNum = 1;
        }, false); 
    }
    else if(opene == true)
    {
        photo.style.animation="in_photoanimation 1s 0s";
        photo.style.animationFillMode = "forwards";
        photo.addEventListener("webkitAnimationEnd", function(){ //动画结束时事件 
            open = true
            opene = false
        }, false); 
    }
}
function mousePhotoOff(){
    var photo = document.getElementById("photo");
    if(open == true)
    {
        photo.style.animation="out_photoanimation 1s 0s";
        photo.style.animationFillMode = "forwards"; 
        photo.addEventListener("webkitAnimationEnd", function(){ //动画结束时事件 
            open = false;
            opene = true;
        }, false); 
    }
}

var number_Search = false;
var Data_Name = [];
var Search_Html = ""

function Search_up(){
    Search_Html = ""
    document.getElementById("search_Content").innerHTML = Search_Html;
    let content_Search = document.getElementById("search_Title").value.toLowerCase();
    
    for(let i = 0; i < Data_Name.length; i++)
    {
        let str = Data_Name[i][0];
        if(content_Search == Data_Name[i][0].toLowerCase())
        {
            Search_Html = "";
            CreateHtml_(Data_Name[i][1],content_Search);
            if(Search_Html != "" && event.keyCode == 13)
            {
                itemTitleOnclick(data[i][1][1],data[i][1][0],data[i][1][2],data[i][0]);
            }
            return;
        }
        else
        {
            if(content_Search.length < Data_Name[i][0].length)
            {
                //console.log(content_Search);
                //console.log(str.substring(0,content_Search.length));
                if(content_Search == str.substring(0,content_Search.length).toLowerCase() && content_Search.length != 0)
                {
                    CreateHtml_(Data_Name[i][1],content_Search);   
                     
                    if(Search_Html != "" && event.keyCode == 13)
                    {
                        itemTitleOnclick(data[i][1][1],data[i][1][0],data[i][1][2],data[i][0]);
                    }           
                }
            }          
        }
    }
   
}


function CreateHtml_(number,content_Search){
    Search_Html += "\n" + 
    `
    <div id="searchItem" style="
        width: 100%;
        height: auto;
        border-radius: 20px;
        background: rgb(219, 219, 219);
        margin-top: 10px;
        padding-bottom: 10px;

        display: grid;
        grid-template-columns: 15% 75%;
    "   onclick=" itemTitleOnclick('`+data[number][1][1]+`','`+data[number][1][0]+`','`+data[number][1][2]+`','`+data[number][0]+`') " >
        <div style="
            margin-top: 15px;
            width: 100%;
            height: 30px;
            text-align: center;
            color: gray;
        "> ` + data[number][1][2] + ` </div>
        <div style="
            margin-top: 15px;
            padding-left: 10px;
            width: 100%;
            height: 100%;
            font-weight: bold;
        ">
            `+ keywordscolorful(data[number][1][0].toLowerCase(),content_Search) +`
        </div>
    </div>
    `
    document.getElementById("search_Content").innerHTML = Search_Html;
}

function Search(){
    Search_Html = "";
    document.getElementById("search_Content").innerHTML = Search_Html;
    document.getElementById("search_Title").value = "";
    if(number_Search == false)
    {
        document.getElementById("search_Box").style.display = "block";
        number_Search = true;
    }
    else{
        document.getElementById("search_Box").style.display = "none";
        number_Search = false;
        return;
    }
    Data_Name = Data_Name.slice(Data_Name.length);
    for(let i = 0; i < data.length; i++)
    {
        Data_Name.push([data[i][1][0],i]);
    }
    document.getElementById("search_Title").focus();
}

function itemTitleOnclick(Read_s,Title_s,Date_s,Classify_s)
{
    localStorage.setItem("Read",Read_s);
    localStorage.setItem("Title",Title_s);
    localStorage.setItem("Date",Date_s);
    localStorage.setItem("Classify",Classify_s);
    window.location.href="/page_file/showBlogs/showBlogs.html";
}
function search_Close(){
    document.getElementById("search_Box").style.display = "none";
}
function keywordscolorful(str, key){
    var reg = new RegExp("(" + key + ")", "g");
    var newstr = str.replace(reg, "<font style='color: rgb(65,105,225);'>$1</font>");
    return newstr;
}
