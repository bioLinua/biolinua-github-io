document.write("<script src='/js_file/Web_App/Web_information.js'></script>");

function Loading(){
    var itemBox_html = "";
    for(var i = 0; i < Data.length; i++)
    {
        itemBox_html += "\n" + 
        `
        <div id="item" onclick="itemOnclick(`+i+`)"  style=" margin-left: 50px;margin-top: 30px;margin-bottom: 30px;">
            <div id="itemPhoto" style="background: url(`+Data[i][1]+`);background-size: cover;">
                <div id="itemTitle">`+Data[i][0]+`</div>
            </div>       
        </div> 
        `
    }
    document.getElementById("itemBox").innerHTML = itemBox_html;

}

window.onload = Loading;

function itemOnclick(number){
    switch(number){
        case 2:
            document.getElementById("search_Box").style.display = "block";
            Search();
            break;
        default:
            localStorage.setItem("Page",document.title);
            window.location.href=Data[number][2];
    }  
}

var number_Search = false;
var Data_Name = [];
var Search_Html = ""


function Search_up(){
    Search_Html = ""
    document.getElementById("search_Content").innerHTML = Search_Html;
    let content_Search = document.getElementById("search_Title").value.toLowerCase();

    for(let i = 0; i < Data_Name.length; i++)
    {
        let str = Data_Name[i][0];
        if(content_Search == Data_Name[i][0].toLowerCase())
        {
            Search_Html = "";
            CreateHtml_(Data_Name[i][1],content_Search);
            if(Search_Html != "" && event.keyCode == 13)
            {
                itemTitleOnclick(i);
            }
            return;
        }
        else
        {
            if(content_Search.length < Data_Name[i][0].length)
            {
                //console.log(content_Search);
                //console.log(str.substring(0,content_Search.length));
                if(content_Search == str.substring(0,content_Search.length).toLowerCase() && content_Search.length != 0)
                {
                    CreateHtml_(Data_Name[i][1],content_Search);       
                    if(Search_Html != "" && event.keyCode == 13)
                    {
                        itemTitleOnclick(i);
                    }       
                }
            }          
        }
    }
}
function CreateHtml_(number,content_Search){
    Search_Html += "\n" + 
    `
    <div id="searchItem" style="
        width: 100%;
        height: auto;
        border-radius: 20px;
        background: rgb(219, 219, 219);
        margin-top: 10px;
        padding-bottom: 10px;

        display: grid;
        grid-template-columns: 15% 75%;
    "   onclick=" itemTitleOnclick('`+ number +`') " >
        <div style="
            margin-top: 15px;
            margin-left: 20px;
            width: 30px;
            height: 30px;
            text-align: center;
            color: gray;
            background: url(`+Data[number][1]+`);
            background-size: cover;
        "></div>
        <div style="
            margin-top: 15px;
            padding-left: 10px;
            width: 100%;
            height: 100%;
            font-weight: bold;
            color: gray;
            text-align: center;
        ">
            `+ keywordscolorful(Data_Name[number][0].toLowerCase(),content_Search) +`
        </div>
    </div>
    `
    document.getElementById("search_Content").innerHTML = Search_Html;
}
function Search(){
    Search_Html = "";
    document.getElementById("search_Content").innerHTML = Search_Html;
    document.getElementById("search_Title").value = "";
    if(number_Search == false)
    {
        document.getElementById("search_Box").style.display = "block";
        number_Search = true;
    }
    else{
        document.getElementById("search_Box").style.display = "none";
        number_Search = false;
        return;
    }
    Data_Name = Data_Name.slice(Data_Name.length);
    for(let i = 0; i < Data.length; i++)
    {
        Data_Name.push([Data[i][0],i]);
    }
    document.getElementById("search_Title").focus();
}
function itemTitleOnclick(Number_)
{
    localStorage.setItem("Page",document.title);
    window.location.href=Data[Number_][2];
}
function search_Close(){
    document.getElementById("search_Box").style.display = "none";
}

function keywordscolorful(str, key){
    var reg = new RegExp("(" + key + ")", "g");
    var newstr = str.replace(reg, "<font style='color: rgb(65,105,225);'>$1</font>");
    return newstr;
}

